#include "smart_hotel.h"
#include "ui_smart_hotel.h"
#include "tab_form.h"
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QStringList>
#include <QMessageBox>
#include <QThread>
#include <QTimer>
#include <QDateTime>
#include <QFile>
#include <QPixmap>
#include <QMouseEvent>
#include <QKeyEvent>
#include <QSound>

/*
 *----------------------------------------------------------------------------------------------------------------------
 *|  [INDEX]  |   [0]    |    [1]    |   [2]    |    [3]    |    [4]    |    [5]    |    [6]    |    [7]    |    [8]    |
 *|  [ROOMID] | IS_EXIST | TAB_INDEX |ALM_ACTIVE|   LIGHT   |TEMPERATURE|MOVING_SENS|  SUM_PWR  |    CONN   | POINT_PWR |
 *|    eg.9   |if not=-1 |0 - 1st in |0-no;1-yes| 0 - 100 % |IN  CELSIUS|0-no move  |sum pwr    |response   |pwr part   |
 *|           |yes->room |tab; 1- 2nd|                                  |0n last 1mn|usage at   |from room  |after      |
 *|           |    number|in tab etc.|                                  |1-detected |in WATTs   |in sec     |point      |
 *|                                                                     |on last 1mn|
 * */

smart_hotel::smart_hotel(QWidget *parent) : QMainWindow(parent), ui(new Ui::smart_hotel)
{
    ui->setupUi(this);
    /*Uzupelnianie tablicy pokoi informacjami o pokojach niedostepnych*/
    for (int i = 0; i<=299; i++){
        rooms_tab[i][0] = -1;
        rooms_tab[i][1] = -1;
    }
    /*Dodanie dostepnych pokoi do listy*/
    Q_FOREACH(QSerialPortInfo port, QSerialPortInfo::availablePorts()) {
            ui->PortName->addItem(port.portName());
    }

    ui->parter_icon->setPixmap(QPixmap(QCoreApplication::applicationDirPath() +"/images/question.png"));
    ui->p1_icon->setPixmap(QPixmap(QCoreApplication::applicationDirPath() +"/images/question.png"));
    ui->p2_icon->setPixmap(QPixmap(QCoreApplication::applicationDirPath() +"/images/question.png"));

    /*Uruchomienie timera ktory ma liczyc 60 sekund bezczynnosci uzytkownika*/
    inactivity->setSingleShot(true);
    inactivity->start(60000);

    /*Wyzwalacze idle*/
    connect(ui->add_room, SIGNAL(clicked()), this, SLOT(set_active()));
    connect(ui->delete_room, SIGNAL(clicked()), this, SLOT(set_active()));
    connect(ui->b_port_open, SIGNAL(clicked()), this, SLOT(set_active()));
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(set_active()));
    connect(ui->pushButton_3, SIGNAL(clicked()), this, SLOT(set_active()));
    connect(ui->room_list2, SIGNAL(pressed(QModelIndex)), this, SLOT(set_active()));
    connect(ui->PortName, SIGNAL(highlighted(int)), this, SLOT(set_active()));
    connect(ui->parter_warnlist, SIGNAL(pressed(QModelIndex)), this, SLOT(set_active()));
    connect(ui->p1_warnlist, SIGNAL(pressed(QModelIndex)), this, SLOT(set_active()));
    connect(ui->p2_warnlist, SIGNAL(pressed(QModelIndex)), this, SLOT(set_active()));

    /*Podlaczenie wyzwalaczy pod akcje*/
    connect(inactivity, SIGNAL(timeout()), this, SLOT(set_inactive()));
    connect(rsynctimer, SIGNAL(timeout()), this, SLOT(ex_th_tick()));
    connect(rsynctimer, SIGNAL(timeout()), this, SLOT(warn_update()));
    connect(ui->add_room, SIGNAL(clicked()), this, SLOT(add_room_clicked()));
    connect(serial, SIGNAL(readyRead()), this, SLOT(leer_serial()));
    connect(ui->delete_room, SIGNAL(clicked()), this, SLOT(delete_room_clicked()));
}
smart_hotel::~smart_hotel()
{
    delete ui;
}

void smart_hotel::leer_serial()
{
    if (leer_getdata.contains("R3DEND")){
        leer_getdata.clear();
    }
    leer_getdata.append(serial->readAll());
    serial->flush();
    leer_getdata.remove(QRegExp("[\\n\\t\\r]"));
    if(leer_getdata != NULL){
        QStringList splitted_data = leer_getdata.split(";", QString::SkipEmptyParts);

        if (splitted_data.count() > 0){
            if (splitted_data.at(0) == "R3DSCEKKA"){
                if (splitted_data.count() == 9 && splitted_data.at(8) == "R3DEND"){
                    for (int i = 1; i <= N-2; i++)
                        leer_prepareddata[i-1] = splitted_data.at(i).toInt();
                    rooms_tab[leer_prepareddata[0]][2] = leer_prepareddata[1];
                    rooms_tab[leer_prepareddata[0]][3] = leer_prepareddata[2];
                    rooms_tab[leer_prepareddata[0]][4] = leer_prepareddata[3];
                    rooms_tab[leer_prepareddata[0]][5] = leer_prepareddata[4];
                    rooms_tab[leer_prepareddata[0]][6] = leer_prepareddata[5];
                    rooms_tab[leer_prepareddata[0]][7] = QDateTime::currentSecsSinceEpoch();
                    rooms_tab[leer_prepareddata[0]][8] = leer_prepareddata[6];
                    on_tab_parter_currentChanged(ui->tab_parter->currentIndex());
                    on_tab_p1_currentChanged(ui->tab_p1->currentIndex());
                    on_tab_p2_currentChanged(ui->tab_p2->currentIndex());
                    QString fname;
                    QFile iconfile;
                    if (rooms_tab[leer_prepareddata[0]][2] == 1){
                        fname = QCoreApplication::applicationDirPath() + "/images/warning_tab.png";
                        iconfile.setFileName(fname);
                    }
                    else
                        fname = QCoreApplication::applicationDirPath() + "/images/tick_tab.png";
                        iconfile.setFileName(fname);
                    if (!iconfile.exists()){
                        QMessageBox messageBox;
                        messageBox.information(0, "Brak szukanego zasobu", "Ikona nie istnieje (lub ścieżka błędna)!");
                    }
                    else {
                        QIcon icon(fname);
                        if (leer_prepareddata[0] < 100)
                            ui->tab_parter->setTabIcon(rooms_tab[leer_prepareddata[0]][1], icon);
                        else if (leer_prepareddata[0] >= 100 && leer_prepareddata[0] < 200)
                            ui->tab_p1->setTabIcon(rooms_tab[leer_prepareddata[0]][1], icon);
                        else if (leer_prepareddata[0] >= 200)
                            ui->tab_p2->setTabIcon(rooms_tab[leer_prepareddata[0]][1], icon);
                    }
                }
            }
        }
    }
    if(leer_getdata != NULL)
        ui->statusBar->showMessage("Odebrano: '" + leer_getdata +"'");
}

void smart_hotel::on_pushButton_clicked()
{
    ui->PortName->clear();
    Q_FOREACH(QSerialPortInfo port, QSerialPortInfo::availablePorts()) {
            ui->PortName->addItem(port.portName());
    }
}

void smart_hotel::on_b_port_open_clicked()
{
    serial->setPortName(ui->PortName->currentText());
    serial->setBaudRate(QSerialPort::Baud19200);
    serial->setDataBits(QSerialPort::Data8);
    serial->setParity(QSerialPort::NoParity);
    serial->setStopBits(QSerialPort::OneStop);
    serial->setFlowControl(QSerialPort::NoFlowControl);

    if (serial->open(QIODevice::ReadWrite)) {
            ui->statusBar->showMessage("Polaczono z COM" + QString::number(ui->PortName->currentIndex()+1));
            ui->b_port_open->setEnabled(false);
            ui->b_port_close->setEnabled(true);
            ui->room_list2->setEnabled(true);
            ui->room_incr->setEnabled(true);
            ui->add_room->setEnabled(true);
            ui->delete_room->setEnabled(true);
            ui->pushButton_3->setEnabled(true);
        } else {
            ui->b_port_open->setEnabled(true);
            ui->b_port_close->setEnabled(false);
            ui->statusBar->showMessage("Błąd: Podany port nie został znaleziony.");
            QMessageBox messageBox;
            messageBox.information(0, "Brak portu", "Nie można odnaleźć określonego portu!");
        }

     serial->clear();
}

void smart_hotel::on_b_port_close_clicked()
{
    serial->close();
    ui->statusBar->showMessage("Zamknięto połączenie");
    ui->b_port_open->setEnabled(true);
    ui->b_port_close->setEnabled(false);
    ui->room_list2->setEnabled(false);
    ui->room_incr->setEnabled(false);
    ui->add_room->setEnabled(false);
    ui->delete_room->setEnabled(false);
    ui->pushButton_3->setEnabled(false);
}

void smart_hotel::on_actionZamknij_triggered()
{
    qApp->exit();
}

void smart_hotel::on_pushButton_3_clicked()
{
    for (int i = 0; i<=299; i++){
        rooms_tab[i][1] = -1;
        rooms_tab[i][2] = -1;
        rooms_tab[i][3] = -1;
        rooms_tab[i][4] = -1;
        rooms_tab[i][5] = -1;
        rooms_tab[i][6] = 0;
        rooms_tab[i][7] = -1;
        rooms_tab[i][8] = 0;
    }
    rsynctimer->stop();
    fast_roomsscan.clear();
    ui->tab_parter->clear();
    ui->tab_p1->clear();
    ui->tab_p2->clear();
    int *tab_indexes = NULL;
    tab_indexes = new int [3];
    for (int i = 0; i<=2; i++)
        tab_indexes[i] = -1;
    for (int i = 0; i<=300; i++){
        if (i < 100){
            if (rooms_tab[i][0] == i) {
                fast_roomsscan.append(i);
                tab_indexes[0]++;
                rooms_tab[i][1] = tab_indexes[0];
                tab_form *r0 = new tab_form(this);
                ui->tab_parter->addTab(r0, "Pokoj #" + QString::number(i));
                QString fname = QCoreApplication::applicationDirPath() + "/images/qmark_tab.png";
                QFile iconfile(fname);
                if (!iconfile.exists()){
                    QMessageBox messageBox;
                    messageBox.information(0, "Brak szukanego zasobu", "Ikona nie istnieje (lub ścieżka błędna)!");
                }
                else {
                    QIcon icon(fname);
                    ui->tab_parter->setTabIcon(tab_indexes[0], icon);
                }
            }
        }
        else if ((i >=100) && (i < 200)){
            if (rooms_tab[i][0] == i) {
                fast_roomsscan.append(i);
                tab_indexes[1]++;
                rooms_tab[i][1] = tab_indexes[1];
                tab_form *r1 = new tab_form(this);
                ui->tab_p1->addTab(r1, "Pokoj #" + QString::number(i));
                QString fname = QCoreApplication::applicationDirPath() + "/images/qmark_tab.png";
                QFile iconfile(fname);
                if (!iconfile.exists()){
                    QMessageBox messageBox;
                    messageBox.information(0, "Brak szukanego zasobu", "Ikona nie istnieje (lub ścieżka błędna)!");
                }
                else {
                    QIcon icon(fname);
                    ui->tab_p1->setTabIcon(tab_indexes[1], icon);
                }
            }
        }
        else if ((i >= 200) && (i < 300)){
            if (rooms_tab[i][0] == i) {
                fast_roomsscan.append(i);
                tab_indexes[2]++;
                rooms_tab[i][1] = tab_indexes[2];
                tab_form *r2 = new tab_form(this);
                ui->tab_p2->addTab(r2, "Pokoj #" + QString::number(i));
                QString fname = QCoreApplication::applicationDirPath() + "/images/qmark_tab.png";
                QFile iconfile(fname);
                if (!iconfile.exists()){
                    QMessageBox messageBox;
                    messageBox.information(0, "Brak szukanego zasobu", "Ikona nie istnieje (lub ścieżka błędna)!");
                }
                else {
                    QIcon icon(fname);
                    ui->tab_p2->setTabIcon(tab_indexes[2], icon);
                }
            }
        }
    }
    rsynctimer->setSingleShot(true);
    rsynctimer->start(1000);
}

void smart_hotel::ex_th_tick(){
    if(!rsynctimer->isActive()){
        if(fast_roomsscan.count() < 15)
            rsynctimer->start(1000);
        else if(fast_roomsscan.count() >= 15 && fast_roomsscan.count() < 30)
            rsynctimer->start(2000);
        else if(fast_roomsscan.count() >= 30 && fast_roomsscan.count() < 50)
            rsynctimer->start(4000);}
        else
            rsynctimer->start(5000);
        scan_rooms();
        on_tab_parter_currentChanged(ui->tab_parter->currentIndex());
        on_tab_p1_currentChanged(ui->tab_p1->currentIndex());
        on_tab_p2_currentChanged(ui->tab_p2->currentIndex());
}

void smart_hotel::scan_rooms(){
    if(serial->isOpen()){
        if(fast_roomsscan.count() > 0){
            for(int i = 0; i < fast_roomsscan.count(); i++){
                QString temp = "R3DSCEKKA;SCAN;" + QString::number(fast_roomsscan.at(i)) + ";R3DEND";
                if (serial->write(temp.toLatin1(), temp.count()) < 0){
                    ui->statusBar->showMessage("Nie mozna wyslac sygnalu skanu do pokoju #" + QString::number(i));
                }
                QApplication::processEvents();
                QThread::msleep(60);
            }
        }
    }
    else{
        for (int i = 0; i<=299; i++){
            rooms_tab[i][7] = -1;
        }
        on_tab_parter_currentChanged(ui->tab_parter->currentIndex());
        on_tab_p1_currentChanged(ui->tab_p1->currentIndex());
        on_tab_p2_currentChanged(ui->tab_p2->currentIndex());
        rsynctimer->stop();
    }
}

void smart_hotel::add_room_clicked()
{
    if (rooms_tab[ui->room_incr->value()][0] != -1) {
        QMessageBox messageBox;
        messageBox.information(0, "Pokój już dodany", "Nie można dodać pokoju, który już istnieje w bazie!");
    }
    else {
        rooms_tab[ui->room_incr->value()][0] = ui->room_incr->value();
        ui->room_list2->addItem(QString::number(ui->room_incr->value()));

        rooms_count = 0;
        for (int i = 0; i<=299; i++) {
            if(rooms_tab[i][0] != -1)
                rooms_count++;
        }

        ui->rooms_cnt->setText(QString::number(rooms_count));
    }
}

void smart_hotel::delete_room_clicked()
{
    if(ui->room_list2->selectedItems().count() != 0){
        if(rooms_tab[ui->room_list2->currentItem()->text().toInt()][0] == ui->room_list2->currentItem()->text().toInt()){
            rooms_tab[ui->room_list2->currentItem()->text().toInt()][0] = -1;
            qDeleteAll(ui->room_list2->selectedItems());
            rooms_count = 0;

            for (int i = 0; i<=299; i++) {
                if(rooms_tab[i][0] != -1)
                    rooms_count++;
            }

            ui->rooms_cnt->setText(QString::number(rooms_count));
        }
    }
    else{
        QMessageBox messageBox;
        messageBox.information(0, "Nie wybrano pokoju", "Aby usunąć pokój należy go najpierw zaznaczyć na liście!");
    }
}

void smart_hotel::on_tab_parter_currentChanged(int index)
{
    int roomid = -1;
    for (int i = 0; i<=99; i++)
        if(rooms_tab[i][1] == index)
            roomid = i;
    if (index >= 0){
        tab_form *tabber = (tab_form*)ui->tab_parter->widget(index);
        tabber->ui_set(roomid, rooms_tab);
        if (rooms_tab[roomid][2] == 1 && QDateTime::currentSecsSinceEpoch() - rooms_tab[roomid][7] <= 10){
            QString fname = QCoreApplication::applicationDirPath() + "/images/warning_tab.png";
            QFile iconfile(fname);
            if (!iconfile.exists()){
                QMessageBox messageBox;
                messageBox.information(0, "Brak szukanego zasobu", "Ikona nie istnieje (lub ścieżka błędna)!");
            }
            else {
                QIcon icon(fname);
                ui->tab_parter->setTabIcon(index, icon);
            }
        }
        else if(rooms_tab[roomid][2] == 0 && QDateTime::currentSecsSinceEpoch() - rooms_tab[roomid][7] <= 10){
            QString fname = QCoreApplication::applicationDirPath() + "/images/tick_tab.png";
            QFile iconfile(fname);
            if (!iconfile.exists()){
                QMessageBox messageBox;
                messageBox.information(0, "Brak szukanego zasobu", "Ikona nie istnieje (lub ścieżka błędna)!");
            }
            else {
                QIcon icon(fname);
                ui->tab_parter->setTabIcon(index, icon);
            }
        }
        else{
            QString fname = QCoreApplication::applicationDirPath() + "/images/qmark_tab.png";
            QFile iconfile(fname);
            if (!iconfile.exists()){
                QMessageBox messageBox;
                messageBox.information(0, "Brak szukanego zasobu", "Ikona nie istnieje (lub ścieżka błędna)!");
            }
            else {
                QIcon icon(fname);
                ui->tab_parter->setTabIcon(index, icon);
            }
        }
    }
}

void smart_hotel::on_tab_p1_currentChanged(int index)
{
    int roomid = -1;
    for (int i = 100; i<=199; i++)
        if(rooms_tab[i][1] == index)
            roomid = i;
    if (index >= 0){
        tab_form *tabber = (tab_form*)ui->tab_p1->widget(index);
        tabber->ui_set(roomid, rooms_tab);
    }
    if (rooms_tab[roomid][2] == 1){
        QString fname = QCoreApplication::applicationDirPath() + "/images/warning_tab.png";
        QFile iconfile(fname);
        if (!iconfile.exists()){
            QMessageBox messageBox;
            messageBox.information(0, "Brak szukanego zasobu", "Ikona nie istnieje (lub ścieżka błędna)!");
        }
        else {
            QIcon icon(fname);
            ui->tab_p1->setTabIcon(index, icon);
        }
    }
    else if(rooms_tab[roomid][2] == 0){
        QString fname = QCoreApplication::applicationDirPath() + "/images/tick_tab.png";
        QFile iconfile(fname);
        if (!iconfile.exists()){
            QMessageBox messageBox;
            messageBox.information(0, "Brak szukanego zasobu", "Ikona nie istnieje (lub ścieżka błędna)!");
        }
        else {
            QIcon icon(fname);
            ui->tab_p1->setTabIcon(index, icon);
        }
    }
    else{
        QString fname = QCoreApplication::applicationDirPath() + "/images/qmark_tab.png";
        QFile iconfile(fname);
        if (!iconfile.exists()){
            QMessageBox messageBox;
            messageBox.information(0, "Brak szukanego zasobu", "Ikona nie istnieje (lub ścieżka błędna)!");
        }
        else {
            QIcon icon(fname);
            ui->tab_p1->setTabIcon(index, icon);
        }
    }
}

void smart_hotel::on_tab_p2_currentChanged(int index)
{
    int roomid = -1;
    for (int i = 200; i<=299; i++)
        if(rooms_tab[i][1] == index)
            roomid = i;
    if (index >= 0){
        tab_form *tabber = (tab_form*)ui->tab_p2->widget(index);
        tabber->ui_set(roomid, rooms_tab);
    }
    if (rooms_tab[roomid][2] == 1){
        QString fname = QCoreApplication::applicationDirPath() + "/images/warning_tab.png";
        QFile iconfile(fname);
        if (!iconfile.exists()){
            QMessageBox messageBox;
            messageBox.information(0, "Brak szukanego zasobu", "Ikona nie istnieje (lub ścieżka błędna)!");
        }
        else {
            QIcon icon(fname);
            ui->tab_p2->setTabIcon(index, icon);
        }
    }
    else if(rooms_tab[roomid][2] == 0){
        QString fname = QCoreApplication::applicationDirPath() + "/images/tick_tab.png";
        QFile iconfile(fname);
        if (!iconfile.exists()){
            QMessageBox messageBox;
            messageBox.information(0, "Brak szukanego zasobu", "Ikona nie istnieje (lub ścieżka błędna)!");
        }
        else {
            QIcon icon(fname);
            ui->tab_p2->setTabIcon(index, icon);
        }
    }
    else{
        QString fname = QCoreApplication::applicationDirPath() + "/images/qmark_tab.png";
        QFile iconfile(fname);
        if (!iconfile.exists()){
            QMessageBox messageBox;
            messageBox.information(0, "Brak szukanego zasobu", "Ikona nie istnieje (lub ścieżka błędna)!");
        }
        else {
            QIcon icon(fname);
            ui->tab_p2->setTabIcon(index, icon);
        }
    }
}

void smart_hotel::set_values(int mod, int value, int roomid){
    QString temp = "R3DSCEKKA;SET;" + QString::number(roomid) + ";" + QString::number(mod) + ";" + QString::number(value) + ";R3DEND";
    if (serial->write(temp.toLatin1(), temp.count()) < 0){
        ui->statusBar->showMessage("Nie mozna wyslac sygnalu skanu do pokoju #" + QString::number(roomid));
    }
}

void smart_hotel::warn_update(){
    for (int i = 0; i < ui->parter_warnlist->count(); i++)
        delete ui->parter_warnlist->takeItem(i);
    for (int i = 0; i < ui->p1_warnlist->count(); i++)
        delete ui->p1_warnlist->takeItem(i);
    for (int i = 0; i < ui->p2_warnlist->count(); i++)
        delete ui->p2_warnlist->takeItem(i);
    ui->parter_icon->setPixmap(QPixmap(QCoreApplication::applicationDirPath() +"/images/tick.png"));
    ui->p1_icon->setPixmap(QPixmap(QCoreApplication::applicationDirPath() +"/images/tick.png"));
    ui->p2_icon->setPixmap(QPixmap(QCoreApplication::applicationDirPath() +"/images/tick.png"));
    for(int i = 0; i < fast_roomsscan.count(); i++){
        if(fast_roomsscan.at(i) < 100){
            if (rooms_tab[fast_roomsscan.at(i)][2] == 1){
                ui->parter_warnlist->insertItem(i, QString::number(fast_roomsscan.at(i)));
                ui->parter_icon->setPixmap(QPixmap(QCoreApplication::applicationDirPath() +"/images/warning.png"));
                if(user_idle_trigger == 1){
                    set_active();
                    inactivity->stop();
                    inactivity->start(IDLE/4);
                    QSound::play(QCoreApplication::applicationDirPath()+ "/audio/alert.wav");
                    if(!mes1->isVisible()){
                        mes1->setWindowTitle("Alarm!");
                        mes1->setText("Czujnik PPOŻ jednego z pokoi zwrócił alarm!!!");
                        QPixmap p;
                        p.load(QCoreApplication::applicationDirPath() +"/images/warning.png");
                        mes1->setIconPixmap(p);
                        mes1->setStandardButtons(QMessageBox::Ok);
                        mes1->exec();
                    }
                }

            }
        }
        else if(fast_roomsscan.at(i) >= 100 && fast_roomsscan.at(i) < 200){
            if (rooms_tab[fast_roomsscan.at(i)][2] == 1){
                ui->p1_warnlist->insertItem(i, QString::number(fast_roomsscan.at(i)));
                ui->p1_icon->setPixmap(QPixmap(QCoreApplication::applicationDirPath() +"/images/warning.png"));
                if(user_idle_trigger == 1){
                    set_active();
                    inactivity->stop();
                    inactivity->start(IDLE/4);
                    QSound::play(QCoreApplication::applicationDirPath()+ "/audio/alert.wav");
                    if(!mes1->isVisible()){
                        mes1->setWindowTitle("Alarm!");
                        mes1->setText("Czujnik PPOŻ jednego z pokoi zwrócił alarm!!!");
                        QPixmap p;
                        p.load(QCoreApplication::applicationDirPath() +"/images/warning.png");
                        mes1->setIconPixmap(p);
                        mes1->setStandardButtons(QMessageBox::Ok);
                        mes1->exec();
                    }
                }
            }
        }
        else if(fast_roomsscan.at(i) >= 200 && fast_roomsscan.at(i) < 300){
            if (rooms_tab[fast_roomsscan.at(i)][2] == 1){
                ui->p2_warnlist->insertItem(i, QString::number(fast_roomsscan.at(i)));
                ui->p2_icon->setPixmap(QPixmap(QCoreApplication::applicationDirPath() +"/images/warning.png"));
                if(user_idle_trigger == 1){
                    set_active();
                    inactivity->stop();
                    inactivity->start(IDLE/4);
                    QSound::play(QCoreApplication::applicationDirPath()+ "/audio/alert.wav");
                    if(!mes1->isVisible()){
                        mes1->setWindowTitle("Alarm!");
                        mes1->setText("Czujnik PPOŻ jednego z pokoi zwrócił alarm!!!");
                        QPixmap p;
                        p.load(QCoreApplication::applicationDirPath() +"/images/warning.png");
                        mes1->setIconPixmap(p);
                        mes1->setStandardButtons(QMessageBox::Ok);
                        mes1->exec();
                    }
                }
            }
        }
    }
}

void smart_hotel::set_inactive(){
    user_idle_trigger = 1;
}

void smart_hotel::set_active(){
    user_idle_trigger = 0;
    inactivity->stop();
    inactivity->start(IDLE);
}

void smart_hotel::mouseReleaseEvent(QMouseEvent*){
    set_active();
}

void smart_hotel::keyPressEvent(QKeyEvent*){
    set_active();
}
