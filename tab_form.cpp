#include "tab_form.h"
#include "ui_tab_form.h"
#include "smart_hotel.h"
#include <QDateTime>
#include <QTimer>

tab_form::tab_form(smart_hotel *parent) :
    QMainWindow(parent),
    ui(new Ui::tab_form)
{
    parent2 = parent;
    ui->setupUi(this);
}

tab_form::~tab_form()
{
    delete ui;
}

void tab_form::ui_set(int roomid, int (*rooms_tab2)[9]){
    ui->rnum->setText(QString::number(roomid));
    rnum = ui->rnum->text().toInt();
    ui->label_4->setText("<html><head/><body><p><span style=\" font-size:11pt; font-weight:600; text-decoration: underline;\">#"+QString::number(roomid) + "</span></p></body></html>");
    /*CONNECTION*/
    if(rooms_tab2[roomid][7] == -1 || QDateTime::currentSecsSinceEpoch() - rooms_tab2[roomid][7] > 10)
        ui->lb_ping->setText("<html><head/><body><p><span style=\" font-weight:600; color:#ff0000;\">Brak połączenia!</span></p></body></html>");
    else
        ui->lb_ping->setText("<html><head/><body><p><span style=\" font-weight:600; color:#00B000;\">Aktywne</span></p></body></html>");
    /*ALARM*/
    if(rooms_tab2[roomid][7] == -1){
        ui->lb_warn->setText("<html><head/><body><p><span style=\" font-weight:600; color:#ffb000;\">Brak połączenia!</span></p></body></html>");
        ui->lb_smsens->setText("<html><head/><body><p><span style=\" font-weight:600; color:#ffb000;\">\?\?\?</span></p></body></html>");
    }
    else if (rooms_tab2[roomid][2] == 0){
        ui->lb_warn->setText("<html><head/><body><p><span style=\" font-weight:600; color:#00B000;\">W czuwaniu</span></p></body></html>");
        ui->lb_smsens->setText("<html><head/><body><p><span style=\" font-weight:600; color:#00B000;\">OK</span></p></body></html>");
    }
    else if (rooms_tab2[roomid][2] == 1){
        ui->lb_warn->setText("<html><head/><body><p><span style=\" font-weight:600; color:#FF0000;\">ALARM!!!</span></p></body></html>");
        ui->lb_smsens->setText("<html><head/><body><p><span style=\" font-weight:600; color:#FF0000;\">!!!</span></p></body></html>");
    }
    /*LIGHTNING*/
    ui->lb_lightllv->setText(QString::number(rooms_tab2[roomid][3])+"%");
    /*TEMPERATURE*/
    ui->lb_templv->setText(QString::number(rooms_tab2[roomid][4])+"°C");
    /*MOVEMENT SENSOR*/
    if(rooms_tab2[roomid][5] == -1)
        ui->lb_movsens->setText("<html><head/><body><p><span style=\" font-weight:600; color:#ffb000;\">\?\?\?</span></p></body></html>");
    else if(rooms_tab2[roomid][5] == 1)
        ui->lb_movsens->setText("Wykryto ruch!");
    else
        ui->lb_movsens->setText("Brak ruchu");
    /*POWER USAGE*/
    float pwr_temp = float(rooms_tab2[roomid][8]);
    pwr_temp = float(rooms_tab2[roomid][6])+(pwr_temp/100);
    if(pwr_temp < 1000)
        ui->lb_pwr->setText("<html><head/><body><p><span style=\" font-size:18pt; font-weight:600; color:#5555ff;\">"
                            ""+QString::number(pwr_temp, 'f', 2)+"W</span></p></body></html>");
    else if (pwr_temp >= 1000 && pwr_temp < 1000000)
        ui->lb_pwr->setText("<html><head/><body><p><span style=\" font-size:18pt; font-weight:600; color:#5555ff;\">"
                            ""+QString::number(pwr_temp/1000, 'f', 3)+"kW</span></p></body></html>");
    else if (pwr_temp >= 1000000 && pwr_temp < 1000000000)
        ui->lb_pwr->setText("<html><head/><body><p><span style=\" font-size:18pt; font-weight:600; color:#5555ff;\">"
                            ""+QString::number(pwr_temp/1000000, 'f', 4)+"kW</span></p></body></html>");
    else if (pwr_temp >= 1000000000)
        ui->lb_pwr->setText("<html><head/><body><p><span style=\" font-size:18pt; font-weight:600; color:#5555ff;\">"
                            ""+QString::number(pwr_temp/1000000000, 'f', 5)+"kW</span></p></body></html>");
}

void tab_form::on_horizontalSlider_valueChanged(int value)
{
    ui->lb_lightset->setText(QString::number(value) + "%");
}

void tab_form::on_horizontalSlider_3_valueChanged(int value)
{
    ui->lb_tempset->setText(QString::number(value) + "°C");
}

void tab_form::on_pb_light_clicked()
{
    parent2->set_values(1, this->ui->horizontalSlider->value(), rnum);
}

void tab_form::on_pb_temp_clicked()
{
    parent2->set_values(2, this->ui->horizontalSlider_3->value(), rnum);
}
